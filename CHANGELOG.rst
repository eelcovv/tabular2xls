=========
Changelog
=========

Version 0.3.5
=============
- Nieuwe optie 'merge_top_row' om multirow toprij goed te processen
- Bugfix op multiindex plus nieuw voorbeeld

Version 0.3.4
=============
- tabular nu met encoding utf-8 ingeladen
- hyperref[link]{content} wordt nu vervangen met content

Version 0.3.2
=============
- cbs kleuren worden herkend en vertaald
- update setup for pyscaffold 4

Version 0.3
===========

- Mogelijkheid om search/replace patterns mee te geven
- Per default worden $\cdot$ door '.' vervangen en $\ast$ door '*'

Version 0.2
===========

- Unit test toegevoegd
- Waarschuwing regex onderdrukt

Version 0.1
===========

- Eerste opzet van de tool
