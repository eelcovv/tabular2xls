===========
tabular2xls
===========


Conveteer een LaTeX tabular file naar een xls


Description
===========

Met deze tool kan je LaTeX tabular files naar excel converteren


.. _pyscaffold-notes:

Note
====

This project has been set up using PyScaffold 4.0.1. For details and usage
information on PyScaffold see https://pyscaffold.org/.
